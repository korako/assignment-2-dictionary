ASM = nasm
ASM_FLAG = -f elf64
LD = ld
.PHONY : clean
main: lib.o main.o dict.o
	ld -o $@ $^

%.o: %.asm
	nasm -f elf64 $< -o $@
clean :
	rm *.o
	rm main
