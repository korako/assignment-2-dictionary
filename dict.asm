%include "lib.inc"

global find_word

%define WORD_SIZE 8

section .text

find_word:
    push rsi
    push rdi
    add rsi, WORD_SIZE
    call string_equals
    pop rdi
    pop rsi
    test rax, rax
    jnz .success
    mov rsi, [rsi]
    test rsi, rsi
    jnz find_word
    xor rax, rax
    ret
    .success:
        mov rax, rsi
        ret

