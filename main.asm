%include "colon.inc"
%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define WORD_SIZE 8
%define BUFFER_SIZE 256

section .rodata
message: db 'Please enter the key:', 0
error_message: db 'Reading error', 0
not_found_message: db 'Key not found', 0
section .bss
input: resb BUFFER_SIZE

section .text
global _start
_start:
    mov rdi, message
    call print_string
    mov rdi, input
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    jz .error
    push rdx
    mov rdi, input
    mov rsi, next;
    call find_word
    test rax, rax
    jz .not_found
    pop rdx
    add rax, WORD_SIZE
    mov rdi, rax
    lea rdi, [rdi + rdx + 1]
    call print_string
    call print_newline
    xor rdi, rdi
    call exit
.not_found:
    mov rdi, not_found_message
    call print_string_error
    call print_newline_error
    mov rdi, 1
    call exit
.error:
    mov rdi, error_message
    call print_string_error
    call print_newline_error
    mov rdi, 1
    call exit
